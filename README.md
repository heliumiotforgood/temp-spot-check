# TempSpotCheck #



### What is TempSpotCheck? ###

Business owners and employers are now required to administer temperature checks to employees, customers, and visitors 
seeking entrance in the early stages of reopening after shelter-in-place due to the **COVID-19** pandemic.

How should temperature checks be administered and how should the temperature readings be collected for data analysis?

**TempSpotCheck** is a project that takes the temperature readings from infrared, thermal, and touchless thermometers 
and uploads the data to a **Helium** cloud dashboard for evaluation.

For instance, subjects with a temperature of over 100 F will be encouraged not to enter.

The **Helium Development Kit** and **Helium Hotspots** will pull readings from commercially available thermometers 
and upload the data to a **Helium** cloud dashboard that flags certain temperatures [over 100 F] 
that would be considered a barrier to entry.